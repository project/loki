<?php

namespace Drupal\Tests\loki\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests the module settings form.
 *
 * @group loki
 */
class LokiSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['loki'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * User with the permissions to edit module settings.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer loki']);
  }

  /**
   * Tests if the module functionality can be enabled from the settings form.
   */
  public function testCanEnableModuleFunctionality(): void {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('loki.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//input[@name="enable" and not(@checked)]');

    $edit = ['enable' => 1];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->elementExists('xpath', '//input[@name="enable" and @checked]');
  }

}
