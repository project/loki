<?php

namespace Drupal\Tests\loki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the module event subscriber.
 *
 * @group loki
 */
class LokiSubscriberTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['loki'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Tests if the event subscriber triggers an error when configured to do so.
   */
  public function testSubscriberTriggersErrorResponse(): void {
    // Configure to always error for anonymous users.
    $config = $this->config('loki.settings');
    $config->set('enable', TRUE);
    $config->set('time_min', '00:00:00');
    $config->set('time_max', '23:59:59');
    $config->set('roles', ['anonymous' => 'anonymous']);
    $config->set('responses', [500 => 500]);
    $config->set('randomness', 100);
    $config->save();

    $this->resetAll();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(500);

    // Disable functionality and ensure page loads as expected.
    $config->set('enable', FALSE);
    $config->save();

    $this->resetAll();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
