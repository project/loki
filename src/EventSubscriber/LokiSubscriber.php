<?php

namespace Drupal\loki\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Page response subscriber to set appropriate headers on anonymous requests.
 */
class LokiSubscriber implements EventSubscriberInterface {

  /**
   * The Loki settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $user;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Current route match.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   Current user.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RouteMatchInterface $route_match,
    TimeInterface $time,
    AccountInterface $user,
  ) {
    $this->config = $config_factory->get('loki.settings');
    $this->routeMatch = $route_match;
    $this->time = $time;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onResponse'];
    return $events;
  }

  /**
   * Throws error responses randomly per Loki settings.
   */
  public function onResponse() {
    // Never trigger an error on the Loki settings route.
    if ($this->routeMatch->getRouteName() === 'loki.settings') {
      return;
    }

    // Check enabled.
    if ((bool) $this->config->get('enable') === FALSE) {
      return;
    }

    // Check randomness factor.
    if (mt_rand(0, 100) > $this->config->get('randomness') ?? 0) {
      return;
    }

    // Check time of day constraints.
    $time_min = $this->config->get('time_min');
    $time_max = $this->config->get('time_max');
    if ($time_min && $time_max) {
      $time = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
      $time_min = DrupalDateTime::createFromFormat('H:i:s', $time_min);
      $time_max = DrupalDateTime::createFromFormat('H:i:s', $time_max);
      if ($time > $time_max && $time < $time_min) {
        return;
      }
    }

    // Check configured roles.
    $user_in_configured_role = (bool) array_intersect(
      $this->config->get('roles') ?? [],
      $this->user->getRoles()
    );
    if (!$user_in_configured_role) {
      return;
    }

    // Trouble!
    $status_code = array_rand($this->config->get('responses')) ?? Response::HTTP_INTERNAL_SERVER_ERROR;
    throw new HttpException($status_code, 'Exception triggered by Loki!');
  }

}
