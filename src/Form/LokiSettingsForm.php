<?php

namespace Drupal\loki\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a general admin form for the Loki module.
 *
 * @package Drupal\loki\Form
 */
class LokiSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a LokiSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'loki_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['loki.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->get('loki.settings');

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Check this box to start making trouble.'),
      '#default_value' => $config->get('enable'),
      '#disabled' => $this->configIsOverridden('enable'),
    ];

    $form['time_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Timing'),
      '#open' => TRUE,
    ];

    $form['time_options']['time_min'] = [
      '#type' => 'datetime',
      '#date_date_element' => 'none',
      '#title' => $this->t('Earliest time of day'),
      '#description' => $this->t('Earliest time of day to make trouble. Based on request time in configured time zone.'),
      '#default_value' => ($config->get('time_min') != NULL
        ? DrupalDateTime::createFromFormat('H:i:s', $config->get('time_min'))
        : NULL),
      '#disabled' => $this->configIsOverridden('time_min'),
    ];

    $form['time_options']['time_max'] = [
      '#type' => 'datetime',
      '#date_date_element' => 'none',
      '#title' => $this->t('Latest time of day'),
      '#description' => $this->t('Latest time of day to make trouble. Based on request time in configured time zone.'),
      '#default_value' => ($config->get('time_max') != NULL
        ? DrupalDateTime::createFromFormat('H:i:s', $config->get('time_max'))
        : NULL),
      '#disabled' => $this->configIsOverridden('time_max'),
    ];

    $form['response_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Error responses'),
      '#open' => TRUE,
    ];

    $form['response_options']['randomness'] = [
      '#type' => 'number',
      '#title' => $this->t('Error response likelihood'),
      '#description' => $this->t('How likely any response is to be an error response, represented as a percentage (0 = no error responses, 50 = even chance of an error response, 100 = every response is an error).'),
      '#step' => 1,
      '#min' => 0,
      '#max' => 100,
      '#size' => 4,
      '#default_value' => $config->get('randomness'),
      '#disabled' => $this->configIsOverridden('randomness'),
    ];

    $response_options = array_filter(Response::$statusTexts, function ($code) {
      return $code % 500 < 100;
    }, ARRAY_FILTER_USE_KEY);
    $form['response_options']['responses'] = [
      '#type' => 'select',
      '#title' => $this->t('Response types'),
      '#description' => $this->t('Response types to trigger from the application.'),
      '#options' => $response_options,
      '#multiple' => TRUE,
      '#size' => count($response_options),
      '#default_value' => $config->get('responses'),
      '#disabled' => $this->configIsOverridden('responses'),
    ];

    $form['role_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#open' => TRUE,
    ];

    $roles = array_map(function (RoleInterface $role) {
      return Html::escape($role->label());
    }, $this->entityTypeManager->getStorage('user_role')->loadMultiple());
    $form['role_options']['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Affected roles'),
      '#description' => $this->t('Make trouble for selected roles (only).'),
      '#options' => $roles,
      '#multiple' => TRUE,
      '#size' => count($roles),
      '#default_value' => $config->get('roles'),
      '#disabled' => $this->configIsOverridden('roles'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $time_min = $form_state->getValue('time_min');
    $time_max = $form_state->getValue('time_max');
    if (count(array_filter([$time_max, $time_min])) === 1) {
      $form_state->setErrorByName(
        'time_options',
        $this->t('<strong>Latest time of day</strong> and <strong>Earliest time of day</strong> are both required.')
      );
    }
    elseif ($time_min > $time_max) {
      $form_state->setErrorByName(
        'time_min',
        $this->t('<strong>Earliest time of day</strong> must be earlier than <strong>Latest time of day</strong>.')
      );
    }

    $randomness = (int) $form_state->getValue('randomness');
    if ($randomness < 0 || $randomness > 100) {
      $form_state->setErrorByName(
        'randomness',
        $this->t('<strong>Error response likelihood</strong> must be between 0 and 100 (inclusive).')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('loki.settings');
    foreach (['enable', 'randomness', 'responses', 'roles'] as $key) {
      if ($this->configIsOverridden($key)) {
        continue;
      }
      $config->set($key, $form_state->getValue($key));
    }

    foreach (['time_min', 'time_max'] as $key) {
      if ($this->configIsOverridden($key)) {
        continue;
      }
      $value = $form_state->getValue($key);
      if ($value instanceof DrupalDateTime) {
        $value = $value->format('H:i:s');
      }
      $config->set($key, $value);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Check if a config variable is overridden by local settings.
   *
   * @param string $name
   *   Settings key to check.
   *
   * @return bool
   *   TRUE if the config is overwritten, FALSE otherwise.
   */
  protected function configIsOverridden(string $name): bool {
    $original = $this->configFactory
      ->getEditable('loki.settings')
      ->get($name);
    $current = $this->configFactory
      ->get('loki.settings')
      ->get($name);
    return $original != $current;
  }

}
