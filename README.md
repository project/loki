CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Functionality
* Use Cases
* Maintainers

INTRODUCTION
-----

Loki allows site builders to configure simulated random server error responses.

### What's a Loki?

Loki is a [trickster god of Norse mythology](https://norse-mythology.org/gods-and-creatures/the-aesir-gods-and-goddesses/loki/) --

> He’s by turns playful, malicious, and helpful, but he’s always irreverent and
> nihilistic.

This module fits that description quite nicely in that it is somewhat playful
and, in a way, malicious but can also be a helpful way to test and verify origin
and edge configurations with a Drupal application.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

Visit Configuration » Development » Loki (`/admin/config/development/loki`) to
enable and configure module settings.

FUNCTIONALITY
-------------

This module allows the site builder to configure a percent chance (per request)
for certain configurable user roles to receive certain configurable error
responses (in the 5XX range) from the server during any response.

USE CASES
---------

The primary use case this module is developed for is specific and ongoing testing of CDN and proxy servers. The functionality of this module is particularly useful for testing [Stale-If-Error](https://datatracker.ietf.org/doc/html/rfc5861) configuration between origin and proxy servers.

MAINTAINERS
-----------

Current maintainers:
* Christopher C. Wells (wells) - https://www.drupal.org/u/wells

This project has been sponsored by:
* Cascade Public Media
